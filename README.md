# Microclass

A few library files for micropython to use in high school engineering classrooms.

Documentation available [here](https://bdelaup.gitlab.io/upy_class_libs).

Enjoy :smile:
