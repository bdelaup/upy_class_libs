---
hide:
  - navigation
---


# Bibliothèques
## Télécharger Micropython 

Télécharger le firmware micropython pour micro:bit v2.1.1 [:simple-micropython:](assets/binaires/micropython-microbit-v2.1.1.hex)

## Utilitaire i2c

Fichier source pour micro:bit : [mb_i2c_utils.py](https://gitlab.com/bdelaup/upy_class_libs/-/raw/main/libs/microbit/mb_i2c_utils.py?inline=false)

``` py title="Initialisation" linenums="1"
from microbit import i2c
from mb_i2c_utils import scan_i2c_sensors
```

``` py title="Usage" linenums="1"
# initialisation du bus avec les paramètre par défaut
i2c.init()

# liste les périphérique I2C raccordés
addresses = scan_i2c_sensors()

# Détection du bus i2c en erreur 
# puis reboot de la carte
if len(addresses) > 5:
    machine.reset()
```


## Driver BMP390 : pression

Fichier source pour micro:bit : [mb_bmp290.py](https://gitlab.com/bdelaup/upy_class_libs/-/raw/main/libs/microbit/mb_bmp390.py?inline=false)

``` py title="Initialisation" linenums="1"
from microbit import i2c
from mb_bmp390 import BMP3XX_I2C
bmp390 = BMP3XX_I2C(i2c, address=0x77)
bmp390.pressure_oversampling = 8
bmp.temperature_oversampling = 2
```

``` py title="Usage" linenums="1"
pression = bmp390.pressure
temperature = bmp390.temperature
altitude = bmp390.altitude
```

## Driver SI1145 : uv

Fichier source pour micro:bit : [mb_si1145.py](https://gitlab.com/bdelaup/upy_class_libs/-/raw/main/libs/microbit/mb_si1145.py?inline=false)

``` py title="Initialisation" linenums="1"
from microbit import i2c
from mb_si1145 import SI1145
si1145 = SI1145(i2c=i2c, address=0x60)
```

``` py title="Usage" linenums="1"
uv = sensor.read_uv()
ir = sensor.read_ir()
view = sensor.read_visible()
```

## Driver APC220 : Radio

Fichier source pour micro:bit : [mb_apc220.py](https://gitlab.com/bdelaup/upy_class_libs/-/raw/main/libs/microbit/mb_apc220.py?inline=false)

``` py title="Initialisation" linenums="1"
from microbit import uart, reset
from microbit import pin14, pin15
from mb_apc220 import Apc220, uart_reset

# Ouvre une fenètre de reconfiguration de l'UART 
# pour récupérer la console en cas de problème
uart_reset() 

apc = Apc220(tx=pin14, rx=pin15)
```

``` py title="Usage" linenums="1"
# Envoi de données
apc.send("Radio", "APC", 220)    

# Reception, attente pendant 1000 ms
val = apc.receive(1000)
if val != None:
    print ("Valeur reçue : ", val)
```

## Driver Sen0322 : Oxygen

Fichier source pour micro:bit : [mb_sen0322.py](https://gitlab.com/bdelaup/upy_class_libs/-/raw/main/libs/microbit/mb_sen0322.py?inline=false)

``` py title="Initialisation" linenums="1"
import time
from microbit import i2c
from mb_sen0322 import Sen0322

oxygen_sensor = Sen0322(i2c, address=0x73)

# La concentration d'oxygen dans l'atmosphere est 20.94%
# La calibration n'est pas nécessaire 
# oxygen_sensor.calibrate(20.94)

```

``` py title="Usage" linenums="1"
# Lecture de la concentration d'oxygène
oxygen = oxygen_sensor.read_oxygen()

# Affiche jusqu'à 4 chiffre avant la virgule, 2 chiffres après
print("oxygen concentration is %4.2f %%vol"%oxygen)
```
## Driver DHT20 : Humidité

Fichier source pour micro:bit : [mb_dht20.py](https://gitlab.com/bdelaup/upy_class_libs/-/raw/main/libs/microbit/mb_dht20.py?inline=false)

``` py title="Initialisation" linenums="1"
from microbit import i2c
import time

dht20 = Dht20(i2c, address=0x38)

```

``` py title="Usage" linenums="1"
# Lecture de l'humidité

valeur_humidite = dht20.humidite
print("humidite :", valeur_humidite, "%")

```
## Driver SIM28 ou AIR530 : GPS

Fichier source pour micro:bit : [mb_gps.py](https://gitlab.com/bdelaup/upy_class_libs/-/raw/main/libs/microbit/mb_gps.py?inline=false)

``` py title="Initialisation" linenums="1"
from microbit import uart, reset
from microbit import pin14, pin15
from mb_gps import Gps, uart_reset

# Ouvre une fenètre de reconfiguration de l'UART 
# pour récupérer la console en cas de problème
uart_reset()

gps = Gps(tx=pin15, rx=pin1)

```

``` py title="Usage" linenums="1"
# Affichage 
print(gps.latitude, gps.longitude, gps.altitude)
```

**Exemple:**

Saint etienne le molard 42 => *"4543.81023,N" "00404.79479,E"*

Utiliser :

- [https://www.gpsvisualizer.com](https://www.gpsvisualizer.com) pour visualiser
- [https://www.gpsvisualizer.com/convert_input?convert_format=gpx](https://www.gpsvisualizer.com/convert_input?convert_format=gpx) pour transformer en GPX

``` title="Format" linenums="1"
latitude, longitude
4543.81023, 00404.79479
``` 

