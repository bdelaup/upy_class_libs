---
hide:
  - navigation
---


# Commandes utiles

## Liens

* Éditeur / simulateur officiel : [https://python.microbit.org/v/3](https://python.microbit.org/v/3)
* Documentation : [https://microbit-micropython.readthedocs.io/en/v2-docs/](https://microbit-micropython.readthedocs.io/en/v2-docs/)

## Log (enregistrement)


Documentation : [Data logging](https://microbit-micropython.readthedocs.io/en/v2-docs/log.html?highlight=log)

``` py title="Initialisation" linenums="1"
import log

# Définit les titre des colones
log.set_labels('Nom', "Age", timestamp=log.SECONDS)

# Affiche une copie de l'enregistrement dans la console
log.set_mirroring(True)
```

``` py title="Usage" linenums="1"
# Suppression des logs 
log.delete()

# Ajoute un ligne
log.add(Nom="Luke", Age = 25)
```

## Lecture de broche

Documentation : [Data logging](https://microbit-micropython.readthedocs.io/en/v2-docs/pin.html)

``` py title="Initialisation" linenums="1"
from microbit import pin0, pin8, button_a

# Active la resistance de "pull down" pour la lecture logique
pin8.set_pull(pin8.PULL_DOWN)
```

``` py title="Usage" linenums="1"
# Retourne vrai si la broche est sous tension (état logique haut)
etat = pin8.read_digital()

# retourne une valeur image de la tension appliqué
# Valeur entre 0 et 1023
U0 = pin0.read_analog() 

button_a.is_pressed() # actuellement pressé ?
button_a.was_pressed() # a été pressé ?

```
## Fonction log

Documentation : [Module math de python](https://docs.python.org/fr/3/library/math.html)

``` py title="Usage" linenums="1"
import math
val = math.log(10)
```

