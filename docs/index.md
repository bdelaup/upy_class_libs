---
hide:
  - navigation
#   - toc
---

# 🛖 Accueil
## Description

Ce site est à destination d'étudiant utilisant Micropython. 
![Micropython](img/micropython-logo.png){width=150 align=right}

## Liens utils
### Python, micropython, microbit, SNT

Site dédié à la prise en main de python, notamment avec la carte microbit.

[https://bdelaup.gitlab.io/formation-python-snt :fontawesome-brands-square-gitlab: ](https://bdelaup.gitlab.io/formation-python-snt)

### unet

Unet est une bibliothèque réseau à destination des cartes microbit[^microbit] qui implémente un protocole réseau en 4 couches :

- physique
- réseau
- transport
- application

Il dispose d'une ouverture sur internet via l'implémentation du protocol MQTT sur un noeud connecté au wifi.


[https://bdelaup.gitlab.io/unet/ :fontawesome-brands-square-gitlab:](https://bdelaup.gitlab.io/unet/)





