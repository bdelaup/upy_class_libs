# -*- coding:utf-8 -*-
"""
  *@author [fengli](li.feng@dfrobot.com)
  *@https://github.com/DFRobot/DFRobot_DHT200
  Adapted for microbit by bdelaup
"""

import time

                
class Dht20(object):
    ''' Conversion data '''
    _addr      =  0x50

    def __init__(self ,i2c: I2C ,address=0x38 , debug=False):
        self._i2c = i2c
        self._addr = address
        self.idle =    0
        self._debug = debug
        time.sleep(0.5)
        data = self.read_reg(0x71,1)

        if (data[0] | 0x08) == 0:
          raise Exception("Dht20 : erreur d'initialisation")

    '''
    @brief Get ambient temperature, unit: °C
    @return ambient temperature,the measurement range is -40°C ~ 80°C
    '''
    def get_temperature(self):
        try:
            self.write_reg(0xac,[0x33,0x00])
            time.sleep(0.1)
            data = self.read_reg(0x71,7)
            rawData = ((data[3]&0xf) <<16) + (data[4]<<8)+data[5]
            temperature = float(rawData)/5242 -50
        except:
            print("Erreur sur get_temperature")
            temperature = 0
        return temperature
     
    '''
    @brief Get relative humidity, unit: %RH. 
    @return relative humidity, the measurement range is (1-100%)
    '''
    def get_humidity(self):
        try:
            self.write_reg(0xac,[0x33,0x00])
            time.sleep(0.1)
            data = self.read_reg(0x71,7)
            rawData = ((data[3]&0xf0) >>4) + (data[1]<<12)+(data[2]<<4)
            humidity = float(rawData)/0x100000
        except:
            print("Erreur sur get_temperature")
            humidity = 0
        return humidity*100
    
    @property
    def humidite(self) -> str:
        return self.get_humidity()

    def write_reg(self, register, data):
        self._i2c.write(self._addr, bytearray([register]+data))

    def read_reg(self, register, length):

        self._i2c.write(self._addr, bytearray([register]))
        rslt = self._i2c.read(self._addr, length)
        return rslt

if __name__ == "__main__":
    from microbit import i2c
    import time
 
    dht20 = Dht20(i2c, address=0x38 , debug=True)

    while True:
#         print("temperature :", dht20.get_temperature(), "°C", "humidite :", dht20.get_humidity(), "%")
        print("temperature :", dht20.humidite, "%")
        time.sleep(1)


