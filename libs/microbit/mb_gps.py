from microbit import *
import time

def read_char():
    while not uart.any() :
        pass
    return uart.read(1)
    

class Gps:
    def __init__(self, tx, rx):
        self.tx = tx
        self.rx = rx
        
    def receive_GxGGA(self):
        uart.init(9600, tx=self.tx , rx=self.rx)
        tag = ""
        while tag != "$GPGGA" and tag != "$GNGGA":
            char = b''
            while char != b'$':
                char = read_char()
            frame = char
            cnt=0
            char = b''
            while cnt < 128 and char !=b'\n':
                cnt+=1
                char = read_char()
                frame+=char
            try:
                tag = frame.decode('utf-8').split(',')[0]
                value = frame.decode('utf-8').split(',')[1:]
            except:
                pass

        uart.init(115200)
        return value
    
    @property
    def latitude(self) -> str:
        frame = self.receive_GxGGA()
        if frame[1] == '':
            return '0.0'
        else:
            return frame[1]
    
    @property
    def longitude(self) -> str:
        frame = self.receive_GxGGA()
        if frame[3] == '':
            return '0.0'
        else:
            return frame[3]
    
    @property
    def altitude(self) -> str:
        frame = self.receive_GxGGA()
        if frame[8] == '':
            return '0.0'
        else:
            return frame[8]
        
    def receive_all(self):
        uart.init(9600, tx=self.tx , rx=self.rx)
        ret = {}
        for i in range(10):
            char = b''
            while char != b'$':
                char = read_char()
            frame = char
            cnt=0
            char = b''
            while cnt < 128 and char !=b'\n':
                cnt+=1
                char = read_char()
                frame+=char
            try:
                tag = frame.decode('utf-8').split(',')[0]
                ret[tag] = frame
            except:
                ret[i] = "Error"

        uart.init(115200)
        return ret

def uart_reset():
    uart.init(115200)
    print("=== UART - reset window open ===")
    sleep(1000)
    print("=== UART - reset window close ===")
        
if __name__ == "__main__":
    uart_reset()
    gps = Gps(tx=pin15, rx=pin1)
    
    while (True):
        # Saint etienne le molard 42 => "4543.81023,N" "00404.79479,E"
        # Utiliser https://www.gpsvisualizer.com pour visualiser
        # Utiliser https://www.gpsvisualizer.com/convert_input?convert_format=gpx pour transformer en GPX
        # Format :
        # latitude, longitude
        # 4543.81023, 00404.79479
        print(gps.latitude, gps.longitude, gps.altitude)
#         vals = gps.receive_all()
#         for k,v in vals.items():
#             print(k, v)
        time.sleep(1)
    



