from time import sleep_ms

import machine

from microbit import i2c, uart, reset
from microbit import display, Image
from microbit import pin0, pin8, pin14, pin15, button_a

from mb_i2c_utils import scan_i2c_sensors
from mb_bmp390 import BMP3XX_I2C
from mb_si1145 import SI1145
from mb_apc220 import Apc220, uart_reset

import log
import math

ACTIF_BMP390 = True
ACTIF_SI1145 = False
ACTIF_APC220 = True
ACTIF_THERMISTANCE = True
ACTIF_P8_DETECTION = True
ACTIF_LOG = True


# Initialisation de l'enregistrement
if ACTIF_LOG:
    if button_a.is_pressed():
        display.show(Image.NO)
        log.delete()
        print ("Log effacé")
        sleep_ms(500)
    log.set_labels('Pression', 'Temp_bmp', 'UV', "IR", "VISIBLE", "Temp_th", timestamp=log.SECONDS)
    log.set_mirroring(True)

# Intialisation des broches
display.show(Image.CLOCK12)
if ACTIF_P8_DETECTION:
    pin8.set_pull(pin8.PULL_DOWN)

# Initialisation des capteurs
display.show(Image.CLOCK3)
i2c.init()
addresses = scan_i2c_sensors()
if len(addresses) > 3:
    machine.reset()
sleep_ms(500)

# Capteur UV
display.show(Image.CLOCK6)
if ACTIF_SI1145:
    print("si1145 initialisation")
    si1145 = SI1145(i2c=i2c)
else :
    si1145 = None
sleep_ms(500)
    
# Capteur pression
display.show(Image.CLOCK9)
if ACTIF_BMP390:
    print("bmp390 initialisation")
    bmp390 = BMP3XX_I2C(i2c)
    bmp390.pressure_oversampling = 8
    bmp390.temperature_oversampling = 2
else :
    bmp390 = None
sleep_ms(500)

# Radio APC220
display.show(Image.CLOCK12)
if ACTIF_APC220:
    print("Initialisation APC220")
    uart_reset()
    apc = Apc220(tx=pin14, rx=pin15)
    apc.send("APC220")    
    val = apc.receive(1000)
    if val != None:
        print ("<= ", val)
sleep_ms(500)
    
# Attente largage
if ACTIF_P8_DETECTION :
    display.show(Image.ASLEEP)    
    while pin8.read_digital() == True:
        print("Attente de détection du largage")
        sleep_ms(1000)
    print("Largué")

isplay.clear()

while True:
    display.set_pixel(0,0,1)
    
    # Lecture pression
    if ACTIF_BMP390:
        pression = bmp390.pressure
        temperature_bmp390 = bmp390.temperature

    else :
        pression, temperature_bmp390 = "x", "x"
        
    # Lecture UV
    if ACTIF_SI1145:
        uv = si1145.uv
        ir = si1145.ir
        visible = si1145.visible
    else:
        uv, ir, visible = "x", "x", "x"
    
    # Lecture thermistance
    if ACTIF_THERMISTANCE :
        B = 4300  
        U0 = pin0.read_analog()           
        temperature_thermistance = 1/(math.log((1023/U0)-1)/B+1/298.15)-273.15
    else:
        temperature_thermistance = "x"
    sleep_ms(500)
    
    display.clear()
    
    if ACTIF_LOG :
        log.add(Pression=pression,
                Temp_bmp=temperature_bmp390,
                UV = uv,
                IR = ir,
                VISIBLE = visible,
                Temp_th = temperature_thermistance
                )
    
    print("BMP390 >" , "Pression : ", pression, " Temperature : ", temperature_bmp390)
    print("ACTIF_SI1145 >" , "uv : ", uv, " ir : ", ir, " Visible : ", visible)
    print("Thermistance >" , "Temperature : ", temperature_thermistance)
    print()
    
    if ACTIF_APC220 :
        apc.send("BMP390 >" , "Pression : ", pression, " Temperature : ", temperature_bmp390)
        apc.send("ACTIF_SI1145 >" , "uv : ", uv, " ir : ", ir, " Visible : ", visible)
        apc.send("Thermistance >" , "Temperature : ", temperature_thermistance)
        apc.send("")
    sleep_ms(500)
    

