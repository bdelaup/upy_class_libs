from microbit import i2c, display, Image, uart

import log

import time

from mb_i2c_utils import scan_i2c_sensors
from mb_bmp390 import BMP3XX_I2C
from mb_si1145 import SI1145



log.set_labels('Pression', 'Temp', 'UV', "IR", "VISIBLE", timestamp=log.SECONDS)
log.set_mirroring(True)


scan_i2c_sensors()

bmp390 = BMP3XX_I2C(i2c)
bmp390.pressure_oversampling = 8
bmp390.temperature_oversampling = 2

si1145 = SI1145(i2c=i2c)



while True:
    display.show(Image.HEART)
    print("Pression: {:6.4f}  Température: {:5.2f}".format(bmp390.pressure, bmp390.temperature))
    print("UV: {} IR: {} Visible: {}".format(si1145.uv, si1145.ir, si1145.visible))
    time.sleep(1)
    
    display.show(Image.HEART_SMALL)
    log.add(Pression=bmp390.pressure, Temp=bmp390.temperature, UV = si1145.uv, IR = si1145.ir, VISIBLE = si1145.visible)
    time.sleep(1)


